const { verifyToken } = require( '../helpers/bcrypt' )

const auth = async ( req, res, next ) => {
    // Leer el token del header
    const token = req.header( "x-token" );
    // Revisar si hay token
    if ( !token ) {
        return res.status( 409 ).json( { msg: "Acceso no autorizado" } );
    }
    // Verify token
    try {
        const algo = await verifyToken( token )
        req.user = algo
        next()
    } catch ( err ) {
        console.error( "algo paso mal con el middleware" );
        res.status( 500 ).json( { msg: "Error del Servidor" } );
    }
};

module.exports = { auth }