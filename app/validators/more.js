const { check } = require("express-validator"); //TODO <---
const { validateResult } = require("../helpers/validateHelper");

const validateCreate = [
  //TODO:name, age, email
  check("edad")
    .notEmpty()
    .withMessage("Edad es requerido")
    .trim()
    .isLength({ min: 2 })
    .withMessage("Apodo debe ser de almenos 2 digitos"),
  check("cumple").notEmpty().withMessage("Cumpleanios es requerido").trim(),
  check("email")
    .notEmpty()
    .withMessage("Email es requerido")
    .trim()
    .isEmail()
    .withMessage("Formato de email invalido"),
  (req, res, next) => {
    validateResult(req, res, next);
  },
];

module.exports = { validateCreate };
