const { check } = require("express-validator"); //TODO <---
const { validateResult } = require("../helpers/validateHelper");

const validateCreate = [
  //TODO:name, age, email
  check("apodo")
    .notEmpty()
    .withMessage("Apodo es requerido")
    .trim()
    .isLength({ min: 4 })
    .withMessage("Apodo debe ser de almenos 4 caracteres"),
  check("celular")
    .notEmpty()
    .withMessage("Celular es requerido")
    .trim()
    .isLength({ min: 10 })
    .withMessage("Celular debe ser de almenos 10 caracteres"),
  check("password")
    .notEmpty()
    .withMessage("Password is required")
    .isLength({ min: 6 })
    .withMessage("Password must be at least 6 characters long"),
  (req, res, next) => {
    validateResult(req, res, next);
  },
];

module.exports = { validateCreate };
