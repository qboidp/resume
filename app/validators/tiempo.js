const { check } = require("express-validator"); //TODO <---
const { validateResult } = require("../helpers/validateHelper");

const validateCreate = [
  //TODO:name, age, email
  check("nombre")
    .notEmpty()
    .withMessage("Nombre es requerido")
    .trim(),
  (req, res, next) => {
    validateResult(req, res, next);
  },
];

module.exports = { validateCreate };
