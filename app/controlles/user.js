const { hashPass, tokenSign } = require( "../helpers/bcrypt" );
const { httpError } = require( "../helpers/error" );
const User = require( '../models/User' )

const registerCtl = async ( req, res ) => {
  try {
    const { celular, pass } = req.body;
    let x = new User();
    x.celular = celular
    x.pass = await hashPass( pass );
    await x.save();
    const token = await tokenSign( x );
    return res.status( 200 ).json( { token } );
  } catch ( e ) {
    httpError( res, e );
  }
};
const getCtl = async ( req, res ) => {
  try {
    const x = await User.find().sort( { fecha: -1 } );
    if ( !x ) {
      httpError( res, { e: "Ocurrio un error" } );
    }
    return res.status( 200 ).json( x );
  } catch ( e ) {
    httpError( res, e );
  }
};
const putCtl = async ( req, res ) => {
  try {
    const i = req.params.i;
    const x = await User.findByIdAndUpdate( req.params.id, req.body, {
      new: true,
    } ).select( '-pass' );
    res.status( 200 ).json( { e: x } );
  } catch ( e ) {
    httpError( res, e );
  }
};
const delCtl = async ( req, res ) => {
  try {
    const x = await User.findById( req.params.id );
    if ( !x ) {
      return httpError( res, { e: "Ocurrio algo" } );
    }
    if ( x._id.toString() !== req.params.id ) {
      return httpError( res, { e: "Ocurrio algo" } );
    }
    await x.remove();
    return res.status( 200 ).json( { e: x } );
  } catch ( e ) {
    httpError( res, e );
  }
};

module.exports = {
  getCtl,
  registerCtl,
  delCtl,
  putCtl,
};
