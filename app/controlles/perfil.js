const { handleNew } = require( "../helpers/mesta" );
const { hashPass } = require( "../helpers/bcrypt" );
const { httpError } = require( "../helpers/error" );
const d = require( '../models/Personal/datos' );
const Pro = require( '../models/Personal/Profesion' );
const Chamba = require( "../models/Personal/Chamba" );
const Proyecto = require( '../models/Personal/Proyecto' );
const Perfil = require( "../models/Perfil" )

const postCtl = async ( req, res ) => {
  try {
    const { perfil, profesional, chamba, proyecto } = req.body;
    let x = new Perfil();
    x.perfil = handleNew( perfil, d )
    x.profesional = handleNew( profesional, Pro )
    x.chamba = handleNew( chamba, Chamba )
    x.proyecto = handleNew( proyecto, Proyecto )
    await x.save();
    res.status( 200 ).json( { e: x } );
  } catch ( e ) {
    httpError( res, e );
  }
};
const getCtl = async ( req, res ) => {
  try {
    const y = await Perfil.find().sort( { fecha: -1 } );
    if ( !y ) {
      httpError( res, { e: "Ocurrio un error" } );
    }
    return res.status( 200 ).json( { x: y[ 0 ] } );
  } catch ( e ) {
    httpError( res, e );
  }
};

module.exports = {
  getCtl,
  postCtl
};
