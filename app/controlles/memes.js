const { handleBB } = require( "../helpers/bb" );
const { httpError } = require( "../helpers/error" );
const Meme = require( '../models/Memes' );
const User = require( '../models/User' );

const getCtl = async ( req, res ) => {
    try {
        const x = await Meme.find().sort( { fecha: -1 } );
        if ( !x ) {
            httpError( res, { e: "Ocurrio un error" } );
        }
        return res.status( 200 ).json( x );
    } catch ( e ) {
        httpError( res, e );
    }
};

const postCtl = async ( req, res ) => {//
    try {
        const { file, text } = req.body;
        let x = new Meme( req.body );
        x.texto = text;
        file !== null && file !== "null" ? x.imagen = await handleBB( req.files ) : null
        await x.save();
        res.status( 200 ).json( { e: x } );
    } catch ( e ) {
        httpError( res, e );
    }
};

const putCtl = async ( req, res ) => {
    try {
        const { file, texto } = req.body;
        const { id } = req.params;
        let imagen = ( file !== null && file !== "null" ) ? await handleBB( req.files ) : req.body.imagen;
        const x = await Meme.findByIdAndUpdate( id, { texto, imagen }, { new: true } );
        if ( !x ) {
            httpError( res, { e: "Esto todo loko!" } );
        }
        return res.status( 200 ).json( { e: x } );
    } catch ( e ) {
        httpError( res, e );
    }
};

const delCtl = async ( req, res ) => {
    try {
        const x = await Meme.findById( req.params.id );
        if ( !x ) {
            return httpError( res, { e: "Ocurrio algo" } );
        }
        if ( x._id.toString() !== req.params.id ) {
            return httpError( res, { e: "Ocurrio algo" } );
        }
        await x.remove();
        return res.status( 200 ).json( { e: x } );
    } catch ( e ) {
        httpError( res, e );
    }
};

const masCtl = async ( req, res ) => {
    try {
        const { file, texto } = req.body;
        const me = await Meme.findById( req.params.id ).select( '-pass' );
        const newComment = {
            texto: text ? texto : null,
            imagen: file !== null && file !== "null" ? await handleBB( req.files ) : null
        };
        me.mas.unshift( newComment );
        await me.save();
        res.status( 200 ).json( { e: x.mas } );
    } catch ( err ) {
        console.error( err.message );
        res.status( 500 ).send( 'Server Error' );
    }
}

module.exports = {
    getCtl,
    postCtl,
    delCtl,
    putCtl,
    masCtl,
};
