require("dotenv").config({ path: "variables.env" });
const { httpError } = require("../helpers/handleError");
const { API } = require("../helpers/axios");
const User = require("../models/Users");
const Tiempo = require("../models/Tiempo");
const Social = require("../models/Social");
const More = require("../models/More");
const Idioma = require("../models/Idioma");
const Entidad = require("../models/Entidad");
const fs = require("fs");
const path = require("path");
const FormData = require("form-data");

const allCtrl = async (req, res) => {
  try {
    const ftx = await User.find().sort({ date: -1 });
    if (!ftx) {
      return res.status(400).json({ error: "Ocurrio un error" });
    }
    return res.status(200).json(ftx);
  } catch (e) {
    httpError(res, e);
  }
};

const getCtrl = async (req, res) => {
  try {
    const user = await User.findById(req.params.id)
      .sort({ date: -1 })
      .select("-__v")
      .select("-password");
    if (!user) {
      return res.status(400).json({ error: "Ocurrio un error" });
    }
    return res.status(200).json(user);
  } catch (e) {
    httpError(res, e);
  }
};

const putCtrl = async (req, res) => {
  try {
    const ftx = await User.findByIdAndUpdate(req.user.id, req.body, {
      new: true,
    }).select("-password");
    if (!ftx) {
      return res.status(400).json({ error: "Ocurrio un error" });
    }
    return res.status(200).json(ftx);
  } catch (e) {
    httpError(res, e);
  }
};

const borrarCtrl = async (req, res) => {
  try {
    const user = await User.findById(req.user.id);
    if (!user) {
      return res.status(404).json({ error: "Usuario no encontrado." });
    }
    if (user._id.toString() !== req.user.id) {
      return res.status(401).json({ error: "Usuario no autorizado." });
    }
    await user.remove();
    return res.status(200).json({ error: "Usuario removido" });
  } catch (e) {
    httpError(res, e);
  }
};

const borrarAdminCtrl = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    if (!user) {
      return res.status(404).json({ error: "Usuario no encontrado." });
    }
    if (req.user.role.toString() !== "admin") {
      return res.status(401).json({ error: "Usuario no autorizado." });
    }
    await user.remove();
    return res.status(200).json({ error: "Usuario removido" });
  } catch (e) {
    httpError(res, e);
  }
};

const likeCtrl = async (req, res) => {
  try {
    console.log(req.user._id, req.params.id);
    const user = await User.findById(req.params.id);
    if (user.likes.some((like) => like.user.toString() === req.user._id)) {
      return res.status(200).json({ error: "Gracias por tu reaccion" });
    }
    user.likes.unshift({ user: req.user._id });
    await user.save();
    return res.status(200).json(user.likes);
  } catch (e) {
    httpError(res, e);
  }
};

const nolikeCtrl = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    if (user.likes.some((like) => like.user.toString() === req.user._id)) {
      user.likes = user.likes.filter(
        ({ user }) => user.toString() !== req.user._id
      );
      await user.save();
      return res.status(200).json(user.likes);
    }
    return res.status(200).json({ error: "No hay reaccion" });
  } catch (e) {
    httpError(res, e);
  }
};

const comentCtrl = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    const newComment = {
      text: req.body.text,
      imagen: req.body.imagen,
      apodo: req.body.apodo,
      user: req.user._id,
    };
    user.post.unshift(newComment);
    await user.save();
    return res.status(200).json(user.post);
  } catch (e) {
    httpError(res, e);
  }
};

const nocomentCtrl = async (req, res) => {
  try {
    // console.log(req.user._id);
    const user = await User.findById(req.user._id);
    // console.log(user);
    const para = user.post.find(
      (post) => post._id.toString() === req.params.post_id
    );
    console.log(para);
    if (!para) {
      return res.status(404).json({ msg: "No existe el comentario" });
    }
    if (para.user.toString() !== req.user._id) {
      return res.status(401).json({ msg: "No autorizado" });
    }
    user.post = user.post.filter(({ id }) => id !== req.params.post_id);
    await user.save();
    return res.status(200).json(user.post);
  } catch (e) {
    httpError(res, e);
  }
};

const allXCtrl = async (req, res) => {
  try {
    const user = await User.find().sort({ date: -1 });
    const more = await More.find()
      .sort({ date: -1 })
      .select("-date")
      .select("-__v");
    const tiempo = await Tiempo.find()
      .sort({ date: -1 })
      .select("-__v")
      .select("-date");
    const social = await Social.find()
      .sort({ date: -1 })
      .select("-__v")
      .select("-date");
    const idioma = await Idioma.find()
      .sort({ date: -1 })
      .select("-__v")
      .select("-date");
    const entidad = await Entidad.find()
      .sort({ date: -1 })
      .select("-__v")
      .select("-date");

    if (user) {
      const paraUser = [];
      for (let i = 0; i < user.length; i++) {
        const paraMore = [];
        const paraTiempo = [];
        const paraSocial = [];
        const paraIdioma = [];
        const paraEntidad = [];
        for (let index = 0; index <= more.length; index++) {
          if (more[index]?.user.toString() === user[i]?._id.toString()) {
            paraMore.push(more[index]);
          }
        }
        for (let index = 0; index <= tiempo.length; index++) {
          if (tiempo[index]?.user.toString() === user[i]?._id.toString()) {
            paraTiempo.push(tiempo[index]);
          }
        }
        for (let index = 0; index <= social.length; index++) {
          if (social[index]?.user.toString() === user[i]?._id.toString()) {
            paraSocial.push(social[index]);
          }
        }
        for (let index = 0; index <= idioma.length; index++) {
          if (idioma[index]?.user.toString() === user[i]?._id.toString()) {
            paraIdioma.push(idioma[index]);
          }
        }
        for (let index = 0; index <= entidad.length; index++) {
          if (entidad[index]?.user.toString() === user[i]?._id.toString()) {
            paraEntidad.push(entidad[index]);
          }
        }
        paraUser.push({
          id: user[i]?._id,
          apodo: user[i]?.apodo,
          celular: user[i]?.celular,
          more: paraMore,
          tiempo: paraTiempo,
          social: paraSocial,
          idioma: paraIdioma,
          entidad: paraEntidad,
        });
      }
      return res.status(200).json(paraUser);
    }
  } catch (e) {
    httpError(res, e);
  }
};

// const postXCtrl = async (req, res) => {
//   if (req.files) {
//     let file_path = req.files.otra.path;
//     let file_split = file_path.split("/");
//     let file_name = file_split[2];
//     let ext_split = file_name.split(".");
//     let file_ext = ext_split[1];
//     try {
//       fs.unlink(file_path, (err) => {
//         console.log("err", err);
//       });
//       return res.status(200).json({ path: file_path, ext: file_ext });
//     } catch (error) {
//       console.log(error);
//       res.status(500).send("Error en el servidor");
//     }
//   } else {
//     return res.status(404).json({ msg: "Ocurrio un error" });
//   }
// };
// const postXCtrl = async (req, res) => {
//   if (req.files) {
//     let file_path = req.files.otra.path;

//     try {
//       const file = fs.createReadStream(file_path);

//       const form = new FormData();
//       form.append("key", process.env.APP_KEY);
//       form.append("image", file);

//       const res = await API.post("/upload", form);
//       if (res) {
//         console.log(res);
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).send("Error en el servidor");
//     }
//   } else {
//     return res.status(404).json({ msg: "Ocurrio un error" });
//   }
// };

module.exports = {
  allCtrl,
  getCtrl,
  putCtrl,
  likeCtrl,
  nolikeCtrl,
  comentCtrl,
  nocomentCtrl,
  borrarCtrl,
  borrarAdminCtrl,
  allXCtrl,
};

// exports.crear = async (req, res) => {
//   const errores = validationResult(req);
//   if (!errores.isEmpty()) {
//     return res.status(400).json({ errores: errores.array() });
//   }
//   let {
//     imagen,
//     nombre,
//     correo,
//     apodo,
//     celular,
//     paterno,
//     puesto,
//     des,
//     materno,
//     pass,
//     birth,
//     status,
//     home,
//     fcon,
//     fcel,
//     sitio,
//   } = req.body;
//   try {
//     let angela = await Ftx.findOne({ apodo });
//     if (angela) return res.status(400).json({ msg: "Ya existe el apodo" });
//     if (!angela) {
//       angela = new Ftx({
//         imagen,
//         nombre,
//         correo,
//         apodo,
//         celular,
//         paterno,
//         puesto,
//         des,
//         materno,
//         pass,
//         birth,
//         status,
//         home,
//         fcon,
//         fcel,
//         sitio,
//       });
//       const salt = await bcryptjs.genSalt(10);
//       angela.pass = await bcryptjs.hash(pass, salt);
//       angela.imagen = imagen;
//       angela.nombre = nombre;
//       angela.correo = correo;
//       angela.apodo = apodo;
//       angela.celular = celular;
//       angela.paterno = paterno;
//       angela.puesto = puesto;
//       angela.correo = correo;
//       angela.des = des;
//       angela.materno = materno;
//       angela.birth = birth;
//       angela.status = status;
//       angela.home = home;
//       angela.fcon = fcon;
//       angela.fcel = fcel;
//       angela.sitio = sitio;
//       await angela.save();
//       const payload = {
//         user: {
//           id: angela.id,
//         },
//       };
//       jwt.sign(
//         payload,
//         process.env.JWT_SECRETO,
//         {
//           expiresIn: 3600 /* 3600 = 1hora */,
//         },
//         (err, token) => {
//           if (err) throw err;
//           return res.status(200).json({ token });
//         }
//       );
//     }
//   } catch (error) {
//     console.log(error);
//     return res.status(400).json({ msg: "Hubo un error, Sobre mi..." });
//   }
// };
// exports.actualizar = async (req, res) => {
//   let {
//     imagen,
//     nombre,
//     apodo,
//     celular,
//     paterno,
//     puesto,
//     correo,
//     des,
//     materno,
//     birth,
//     status,
//     home,
//     fcon,
//     fcel,
//     sitio,
//   } = req.body;
//   try {
//     if (
//       imagen ||
//       nombre ||
//       apodo ||
//       celular ||
//       paterno ||
//       puesto ||
//       correo ||
//       des ||
//       materno ||
//       birth ||
//       status ||
//       home ||
//       fcon ||
//       fcel ||
//       sitio
//     ) {
//       let angela = await Ftx.findByIdAndUpdate(req.params.id, req.body, {
//         new: true,
//       }).select("-pass");
//       if (!angela)
//         return res
//           .status(400)
//           .json({ msg: "No se puedo actualizar en este momento" });
//       return res.status(200).json({ angela });
//     } else {
//       return res.status(400).json({ msg: "Ocurrio un error al actualizar" });
//     }
//   } catch (error) {
//     console.log(error);
//     return res.status(400).send("Error en el servidor");
//   }
// };

// exports.crearExp = async (req,res) =>{
//   let {que, como, cuanto} = req.body;
//   try{
//   let angela = new Exp(req.body);
//   await angela.save();
//   return res.status(200).json('Habilidad, creada correctamente');
//   }catch(error){
//   console.log(error);
//   return res.status(400).send('Hubo un error, Trabajo...');
//   }
// }
// exports.getExp = async (req, res) => {
//   try {
//       const joa = await Exp.find().sort({ date: -1 });
//       if(joa)return res.json(joa);
//       return res.status(200).json([]);
//   } catch (err) {
//       console.error(err.message);
//      return  res.status(500).send('Server Error');
//   }
// }
// exports.putExp = async (req, res) => {
//   let {que, como} = req.body;
//   try {
//       if (que || como || cuanto) {
//           let joa = await Exp.findByIdAndUpdate(req.params.id, req.body, { new: true });
//           console.log("joa", joa);
//           if (!joa) return res.status(404).json({ msg: 'Ocurrio un error exp al actualizar' });
//           return res.json({ joa });
//       } else {
//           return res.status(200).json({ msg: 'Ocurrio un error' })
//       }
//   } catch (error) {
//       console.log(error);
//       return res.status(500).send('Error en el servidor');
//   }
// }
// exports.delExp = async (req, res) => {
//     try {
//         let exp = await Exp.findById(req.params.id);
//         if(!exp){ return res.status(404).json({ msg: 'Exp no encontrado' }); }
//         if(exp.user.toString() !== req.user.id) {
//             return res.status(401).json({ msg: 'Usuario no autorizado' }); }
//         await exp.remove();
//         return res.json({ msg: 'Exp removido' });
//     } catch (err) {
//         console.error(err.message);
//         return res.status(500).send('Server Error'); }
// }

// exports.Crear = async (req, res) => {
//   const errores = validationResult(req);
//   if (!errores.isEmpty()) {
//     return res.status(400).json({ errores: errores.array() });
//   }
//   try {
//     let cliente = new Hobbies(req.body);
//     cliente.user = req.params.ftx;
//     await cliente.save();
//     return res.status(200).json("Pasatiempo, creadx correctamente");
//   } catch (error) {
//     console.log(error);
//     return res.status(400).send("Hubo un error, Trabajo...");
//   }
// };

// exports.Actualizar = async (req, res) => {
//   let { nombre, motivo, tiempo } = req.body;
//   try {
//     if (nombre || motivo || tiempo) {
//       let exp = await Hobbies.findByIdAndUpdate(req.params.id, req.body, {
//         new: true,
//       });
//       if (!exp)
//         return res
//           .status(404)
//           .json({ msg: "Ocurrio un error exp al actualizar" });
//       return res.status(200).json({ exp });
//     } else {
//       return res.status(400).json({ msg: "Ocurrio un error" });
//     }
//   } catch (error) {
//     console.log(error);
//     return res.status(500).send("Error en el servidor");
//   }
// };
// exports.Borrar = async (req, res) => {
//   try {
//     let exp = await Cliente.findById(req.params.id);
//     if (!exp) {
//       return res.status(404).json({ msg: "Exp no encontrado" });
//     }
//     await exp.remove();
//     return res.status(200).json({ msg: "Exp removido" });
//   } catch (err) {
//     console.error(err.message);
//     return res.status(500).send("Server Error");
//   }
// };

// exports.All = async (req, res) => {
//   try {
//     const producto = await Hobbies.find().sort();
//     return res.status(200).json(producto);
//   } catch (err) {
//     console.error(err.message);
//     res.status(500).send("Server Error");
//   }
// };

// exports.crear = async (req, res) => {
//   try{
//    let angela = new Lenguas(req.body);
//    angela.user = req.params.ftx;
//   await angela.save();
//   return res.status(200).json('Trabajo, creado correctamente');
//   }catch(error){
//   console.log(error);
//   return res.status(400).send('Hubo un error, Trabajo...');
//   }
// }
// exports.borrar = async (req, res) => {
//   try {
//       const masisa = await Lenguas.findById(req.params.id);

//       // Pull out comment
//       const idioma = masisa.idioma.find(
//           idioma => idioma.id === req.params.iid
//       );
//       // Make sure comment exists
//       if (!idioma) {
//           return res.status(404).json({ msg: 'No existe, no inventes...' });
//       }
//       // // Check user
//       // if (idioma.user.toString() !== req.user.id) {
//       //     return res.status(401).json({ msg: 'No autorizado' });
//       // }

//       masisa.idioma = masisa.idioma.filter(
//           ({ id }) => id !== req.params.iid
//       );

//       await masisa.save();

//       return res.json(masisa.idioma);
//   } catch (err) {
//       console.error(err.message);
//       return res.status(500).send('Server Error');
//   }
// }
// exports.Actualizar = async (req, res) => {
//   let { foto, tipo } = req.body;
//   try {
//     if (foto || tipo) {
//       let work = await Visual.findByIdAndUpdate(req.params.id, req.body, {
//         new: true,
//       });
//       if (!work)
//         return res
//           .status(404)
//           .json({ msg: "Ocurrio un error, al actualizar Señalamiento." });
//       return res.status(200).json({ work });
//     } else {
//       return res
//         .status(400)
//         .json({ msg: "Ocurrio un error, al actualizar Señalamiento." });
//     }
//   } catch (error) {
//     console.log(error);
//     return res
//       .status(500)
//       .send("Error en el servidor, al actualizar Señalamiento.");
//   }
// };
// exports.All = async (req, res) => {
//   try {
//     const producto = await Lenguas.find().sort({ date: -1 });
//     return res.status(200).json(producto);
//   } catch (err) {
//     console.error(err.message);
//     res.status(500).send("Server Error");
//   }
// };
// exports.crearTrabajo = async (req,res) =>{const errores = validationResult(req);
//   if (!errores.isEmpty()) {
//     return res.status(400).json({ errores: errores.array() });
//   }
//   try{
//    let angela = new Trabajo(req.body);
//    angela.user = req.params.ftx;
//   await angela.save();
//   return res.status(200).json('Trabajo, creado correctamente');
//   }catch(error){
//   console.log(error);
//   return res.status(400).send('Hubo un error, Trabajo...');
//   }
// }
// exports.getTrabajosAll = async (req, res) => {
//   try {
//       const joa = await Trabajo.find().sort({ date: -1 });
//       if(joa)return res.status(200).json(joa);
//       return res.status(404).json([]);
//   } catch (err) {
//       console.error(err.message);
//       return res.status(500).send('Server Error');
//   }
// }
// exports.putTrabajos = async (req, res) => {
//   let {imagen, titulo, sitio, des,fecha,final,roles,tipo} = req.body;
//   try {
//       if (imagen || titulo || sitio || des || fecha ||final || roles || tipo) {
//           let work = await Trabajo.findByIdAndUpdate(req.params.id, req.body, { new: true });
//           if (!work) return res.status(404).json({ msg: 'Ocurrio un error trabajo al actualizar' });
//           return res.status(200).json({ work });
//       } else {
//           return res.status(200).json({ msg: 'Ocurrio un error' })
//       }
//   } catch (error) {
//       console.log(error);
//       return res.status(500).send('Error en el servidor');
//   }
// }
// exports.delTrabajos = async (req, res) => {
//     try {
//         let exp = await Trabajo.findById(req.params.id);
//         if(!exp){ return res.status(404).json({ msg: 'Trabajo no encontrado' }); }
//         if(exp.user.toString() !== req.user.id) {
//             return res.status(401).json({ msg: 'Usuario no autorizado' }); }
//         await exp.remove();
//         return res.status(200).json({ msg: 'Trabajo removido' });
//     } catch (err) {
//         console.error(err.message);
//         return res.status(500).send('Server Error'); }
// }
