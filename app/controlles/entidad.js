require("dotenv").config({ path: "variables.env" });
const { httpError } = require("../helpers/handleError");
const Entidad = require("../models/Entidad");
const handleBB = require("../helpers/handleBB");

const crearEntidad = async (req, res) => {
  try {
    const { nombre, description, inicio, final, cargo, type } = req.body;
    const entidad = await new Entidad({ user: req.user.id });
    entidad.imagen = await handleBB(req.files);
    entidad.nombre = nombre;
    entidad.description = description;
    entidad.inicio = inicio;
    entidad.final = final;
    entidad.cargo = cargo;
    entidad.type = type;
    await entidad.save();
    return res.status(200).json(entidad);
  } catch (e) {
    httpError(res, e);
  }
};

const putEntidad = async (req, res) => {
  try {
    const { nombre, description, inicio, final, cargo, type } = req.body;
    const data = {
      nombre,
      description,
      inicio,
      final,
      cargo,
      type,
      foto: await handleBB(req.files),
    };
    let work = await Entidad.findByIdAndUpdate(req.params.id, data, {
      new: true,
    });
    return res.status(200).send({ data: work });
  } catch (e) {
    httpError(res, e);
  }
};

const borrarEntidad = async (req, res) => {
  try {
    const entidad = await Entidad.findById(req.params.id);
    if (!entidad) {
      return res.status(404).json({ error: "Entidad no encontrado." });
    }
    if (entidad.user.toString() !== req.user.id) {
      return res.status(401).json({ error: "Usuario no autorizado." });
    }
    await entidad.remove();
    return res.status(200).json({ data: entidad });
  } catch (e) {
    httpError(res, e);
  }
};

const getAllEntidad = async (req, res) => {
  try {
    const entidad = await Entidad.find().sort({ date: -1 });
    return res.status(200).send({ data: entidad });
  } catch (e) {
    httpError(res, e);
  }
};

module.exports = {
  crearEntidad,
  putEntidad,
  borrarEntidad,
  getAllEntidad,
};
