const { comparePass, tokenSign } = require( "../helpers/bcrypt" );
const { httpError } = require( "../helpers/error" );
const User = require( '../models/User' );

const authUser = async ( req, res ) => {
    try {
        let { celular, pass } = req.body;
        let data = await User.findOne( { celular } );
        if ( !data ) return res.status( 400 ).json( { msg: 'El usuario no existe' } );
        const correcto = await comparePass( pass, data.pass );
        if ( !correcto ) return res.status( 400 ).json( { msg: 'Contraseña Incorrectx' } )
        const token = await tokenSign( data );
        return res.status( 200 ).json( { token } );
    } catch ( e ) {
        httpError( res, e );
    }
}
// Obtiene que usuario esta autenticado
const userNext = async ( req, res ) => {
    try {
        const user = await User.findById( req.user.id ).select( '-pass' );
        return res.status( 200 ).json( { user } );
    } catch ( e ) {
        httpError( res, e );
    }
}

module.exports = {
    authUser,
    userNext
};
