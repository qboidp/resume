require("dotenv").config({ path: "variables.env" });
const { httpError } = require("../helpers/handleError");
const Tiempo = require("../models/Idioma");

const postCtrl = async (req, res) => {
  try {
    const { nombre, comprension, lectura, escritura } = req.body;
    const user = req.user.id;
    const tiempo = new Tiempo({ user });
    tiempo.nombre = nombre;
    tiempo.comprension = comprension;
    tiempo.lectura = lectura;
    tiempo.escritura = escritura;
    await tiempo.save();
    return res.status(201).json(tiempo);
  } catch (e) {
    httpError(res, e);
  }
};

const allCtrl = async (req, res) => {
  try {
    const tiempo = await Tiempo.find().sort({ date: -1 });
    if (!tiempo) {
      return res.status(400).json({ error: "Ocurrio un error" });
    }
    return res.status(200).json(tiempo);
  } catch (e) {
    httpError(res, e);
  }
};

const getCtrl = async (req, res) => {
  try {
    const tiempo = await Tiempo.findById(req.params.id)
      .sort({ date: -1 })
      .select("-__v");
    if (!tiempo) {
      return res.status(400).json({ error: "Ocurrio un error" });
    }
    return res.status(200).json(tiempo);
  } catch (e) {
    httpError(res, e);
  }
};

const putCtrl = async (req, res) => {
  try {
    const tiempo = await Tiempo.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    if (!tiempo) {
      return res.status(400).json({ error: "Ocurrio un error" });
    }
    return res.status(200).json(tiempo);
  } catch (e) {
    httpError(res, e);
  }
};

const borrarCtrl = async (req, res) => {
  try {
    const tiempo = await Tiempo.findOne(req.params.id);
    if (!tiempo) {
      return res.status(404).json({ error: "Usuario no encontrado." });
    }
    if (tiempo._id.toString() !== req.user.id) {
      return res.status(401).json({ error: "Usuario no autorizado." });
    }
    await tiempo.remove();
    return res.status(200).json({ error: "Usuario removido" });
  } catch (e) {
    httpError(res, e);
  }
};

module.exports = {
  postCtrl,
  allCtrl,
  getCtrl,
  putCtrl,
  borrarCtrl,
};
