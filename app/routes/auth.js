const express = require( 'express' );
const { authUser, userNext } = require( '../controlles/auth' );
const { auth } = require( '../midd/ware' );
const router = express.Router();

router.post( '/', authUser );
router.get( '/', [auth], userNext );

module.exports = router;