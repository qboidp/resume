const express = require( "express" );
const router = express.Router();
const multipart = require( "connect-multiparty" );
const part = multipart( { uploadDir: './uploads/users' } )
const { auth } = require( "../midd/ware" )

const { delCtl, postCtl, putCtl, getCtl, masCtl } = require( "../controlles/memes" );

router.get( "/", getCtl );
router.post( "/", [ part, auth ], postCtl );
router.put( "/:id", [ part, auth ], putCtl );
router.delete( "/:id", [ auth ], delCtl );
router.post( "/mas/:id", [ part, auth ], masCtl );

module.exports = router;
