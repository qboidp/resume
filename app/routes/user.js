const express = require( "express" );
const router = express.Router();
const { registerCtl, getCtl, delCtl, putCtl } = require( "../controlles/user" );
const { auth } = require( "../midd/ware" )
//Registro
router.post( "/", registerCtl );//
router.get( "/", getCtl );//
router.put( "/:id", [ auth ], putCtl );//
router.delete( "/:id", [ auth ], delCtl );//

module.exports = router;
