const express = require("express");
const checkAuth = require("../middleware/auth");
const { validateCreate } = require("../validators/more");
const multipart = require("connect-multiparty");
const part = multipart({ uploadDir: "./uploads/users" });
const router = express.Router();
const {
  postCtrl,
  allCtrl,
  getCtrl,
  putCtrl,
  borrarCtrl,
} = require("../controlles/more");
//post more
router.post("/", [part, validateCreate, checkAuth], postCtrl);
//get all
router.get("/", allCtrl);
//get by id
router.get("/:id", checkAuth, getCtrl);
//put
router.put("/:id", [part, checkAuth], putCtrl);
//delete
router.delete("/:id", checkAuth, borrarCtrl);

module.exports = router;
