const express = require("express");
const checkAuth = require("../middleware/auth");
const multipart = require("connect-multiparty");
const part = multipart({ uploadDir: "./uploads/users" });

const router = express.Router();

const {
  allCtrl,
  getCtrl,
  putCtrl,
  likeCtrl,
  nolikeCtrl,
  comentCtrl,
  nocomentCtrl,
  borrarCtrl,
  borrarAdminCtrl,
  allXCtrl,
} = require("../controlles/Users");
//get by admin
// router.post("/x", [part], postXCtrl);
//get by admin
router.get("/x", allXCtrl);
//get all
router.get("/", allCtrl); //
//get by id
router.get("/:id", checkAuth, getCtrl);
//put
router.put("/", checkAuth, putCtrl);
//delete
router.delete("/", checkAuth, borrarCtrl);
//put like
router.put("/l/:id", checkAuth, likeCtrl);
//put dislike
router.put("/l/:id", checkAuth, nolikeCtrl);
//post comment
router.post("/p/:id", checkAuth, comentCtrl);
//delete comment
router.delete("/p/:id", checkAuth, nocomentCtrl);
//delete by admin
router.delete("/x/:id", checkAuth, borrarAdminCtrl);

module.exports = router;
