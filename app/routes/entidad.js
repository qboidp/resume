const express = require("express");
const checkAuth = require("../middleware/auth");
const multipart = require("connect-multiparty");
const part = multipart({ uploadDir: "./uploads/users" });

const router = express.Router();

const {
  crearEntidad,
  putEntidad,
  borrarEntidad,
  getAllEntidad,
} = require("../controlles/entidad");

router.post("/", [part, checkAuth], crearEntidad);
router.put("/:id", [part, checkAuth], putEntidad);
router.delete("/:id", checkAuth, borrarEntidad);
router.get("/", getAllEntidad);

module.exports = router;
