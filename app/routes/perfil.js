const express = require( "express" );
const router = express.Router();
const { postCtl, getCtl } = require( "../controlles/perfil" );
//Registro
router.post( "/", postCtl );//
//
router.get( "/", getCtl );//

module.exports = router;
