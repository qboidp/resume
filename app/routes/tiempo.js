const express = require("express");
const checkAuth = require("../middleware/auth");
const { validateCreate } = require("../validators/tiempo");
const router = express.Router();
const {
  postCtrl,
  allCtrl,
  getCtrl,
  putCtrl,
  borrarCtrl,
} = require("../controlles/tiempo");
//post more
router.post("/", [validateCreate, checkAuth], postCtrl);
//get all
router.get("/", allCtrl);
//get by id
router.get("/:id", checkAuth, getCtrl);
//put
router.put("/:id", checkAuth, putCtrl);
//delete
router.delete("/:id", checkAuth, borrarCtrl);

module.exports = router;
