const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const EntidadSchema = mongoose.Schema({
  user: { type: Schema.Types.ObjectId, ref: "User", required: true },
  imagen: {
    type: String,
  },
  nombre: {
    type: String,
  },
  cargo: { type: String },
  description: {
    type: String,
  },
  type: {
    type: String,
  },
  inicio: { type: Date },
  final: { type: Date },
  date: { type: Date, default: Date.now },
});
module.exports = mongoose.model("Entidad", EntidadSchema);
