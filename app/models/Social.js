const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SocialSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "User",required: true },
  nombre: { type: String },
  url: { type: String },
  date: { type: Date, default: Date.now },
});
module.exports = mongoose.model("Social", SocialSchema);
