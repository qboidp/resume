const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const Perfil = new Schema( {
  perfil:{type:Array},
  chamba:{type:Array},
  profesional:{type:Array},
  proyecto:{type:Array}
} );
module.exports = mongoose.model( "Perfil", Perfil );
