const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const User = new Schema( {
  celular: { type: Number },
  pass: { type: String },
  fecha: {
    type: Date,
    default: Date.now
  }
} );
module.exports = mongoose.model( "User", User );
