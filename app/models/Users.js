const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  apodo: { type: String, required: true, unique: true, trim: true },
  celular: { type: Number, required: true, unique: true, trim: true },
  password: { type: String, required: true },
  like: [
    {
      user: {
        type: Schema.Types.ObjectId,
        ref: "User",
      },
    },
  ],
  post: [
    {
      user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true,
      },
      apodo: { type: String },
      texto: {
        type: String,
      },
      imagen: {
        type: String,
      },
      mostrar: {
        type: Boolean,
      },
      date: {
        type: Date,
        default: Date.now(),
      },
    },
  ],
  role: { type: String, default: "user" },
  date: { type: Date, default: Date.now },
});
module.exports = mongoose.model("User", UserSchema);
