const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const Meme = new Schema( {
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    imagen: { type: String },
    texto: { type: String },
    fecha: {
        type: Date,
        default: Date.now(),
    },
    mas: [
        {
            user: {
                type: Schema.Types.ObjectId,
                ref: 'User'
            },
            texto: {
                type: String
            },
            imagen: {
                type: String
            },
            fecha: {
                type: Date,
                default: Date.now(),
            },
        }
    ],
} );
module.exports = mongoose.model( "Meme", Meme );
