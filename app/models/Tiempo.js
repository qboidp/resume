const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TiempoSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "User", required: true },
  nombre: { type: String, required: true },
  imagen: {
    type: String,
  },
  date: { type: Date, default: Date.now },
});
module.exports = mongoose.model("Tiempo", TiempoSchema);
