const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MoreSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "User", required: true },
  foto: { type: String },
  nombre: { type: String },
  apellido: { type: String },
  email: { type: String },
  civil: { type: String },
  pais: { type: String },
  estado: { type: String },
  cuidad: { type: String },
  mater: { type: String },
  carrera: { type: String },
  ocupacion: { type: String },
  breve: { type: String },
  edad: { type: Number },
  cumple: { type: Date },
  cv: { type: String },
  date: { type: Date, default: Date.now },
});
module.exports = mongoose.model("More", MoreSchema);
