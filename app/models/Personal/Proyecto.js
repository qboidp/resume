const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Proyecto = new Schema({
  nombre: { type: String },
  fecha: { type: Number },
  funcion: { type: String },
  tags:{type:Array}
});
module.exports = mongoose.model("Proyecto", Proyecto);
