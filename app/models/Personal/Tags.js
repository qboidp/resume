const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Tags = new Schema({
  nombre: { type: String },
  tipo: { type: String }
});
module.exports = mongoose.model("Tags", Tags);
