const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Edu = new Schema({
  nombre: { type: String },
  nivel: { type: String },
  fin: { type: Number }
});
module.exports = mongoose.model("Edu", Edu);
