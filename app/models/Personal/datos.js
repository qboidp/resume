const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const Datos = new Schema( {
  nombre: { type: String },
  apellido: { type: String },
  correo: { type: String },
  fotografia: { type: String },
  apodo: { type: String },
  genero: { type: String },
  salud: { type: String },
  civil: { type: String },
  disponibilidad: { type: Boolean },
  nacimiento: {
    dia: { type: Number },
    mes: { type: Number },
    año: { type: Number },
  },
  domicilio: {
    pais: { type: String },
    estado: { type: String },
    ciudad: { type: String },
    calle: { type: String },
    numero: { type: String },
    colonia: { type: String },
    postal: { type: Number },
  },
  comercial: {
    logo: { type: String },
    idea: { type: String },
    mision: { type: String },
    vision: { type: String }
  },
  tags: { type: Array },
  ref: { type: Array },
  redes: { type: Array },
  educacion: { type: Array },
}

);
module.exports = mongoose.model( "Datos", Datos );
