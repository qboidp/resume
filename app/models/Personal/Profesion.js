const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const Pro = new Schema( {
  nombre: { type: String },
  empresa: { type: String },
  fecha: { type: Number },
  funcion: { type: String },
  desc: { type: String },
  tags: { type: Array },
  idioma: { type: Array }
} );
module.exports = mongoose.model( "Pro", Pro );
