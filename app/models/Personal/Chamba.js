const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Chamba = new Schema({
  empresa: { type: String },
  nombre: { type: String },
  ubicacion: { type: String },
  inicio: { type: Number },
  fecha: { type: Number },
  funcion: { type: String },
  tags:{type:Array}
});

module.exports = mongoose.model("Chamba", Chamba);
