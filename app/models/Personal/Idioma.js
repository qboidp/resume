const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Idioma = new Schema({
  nombre: { type: String },
  porcentaje: { type: Number },
  nivel: { type: String },
});
module.exports = mongoose.model("Idioma", Idioma);
