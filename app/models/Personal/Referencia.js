const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Ref = new Schema({
  nombre: { type: String },
  referencia: { type: String },
  correo: { type: String },
  celular: { type: Number }
});
module.exports = mongoose.model("Ref", Ref);

