const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const IdiomaSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "User",required: true },
  nombre: { type: String },
  comprension: { type: Number },
  lectura: { type: Number },
  escritura: { type: Number },
  date: { type: Date, default: Date.now },
});
module.exports = mongoose.model("Idioma", IdiomaSchema);
