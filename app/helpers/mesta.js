const Tags = require( '../models/Personal/Tags' );
const Edu = require( '../models/Personal/Educacion' );
const Redes = require( '../models/Personal/Redes' );
const Idioma = require( '../models/Personal/Idioma' );
const Ref = require( '../models/Personal/Referencia' );

const handleNew = ( arr, f ) => {
  let r = [];
  for ( let a = 0; a < arr.length; a++ ) {
    let y = new f( arr[ a ] );
    if ( arr[ a ].tags ) {
      const { tags } = arr[ a ];
      y.tags = handleMore( tags, Tags );
      if ( arr[ a ].ref && arr[ a ].redes && arr[ a ].educacion ) {
        const { ref, redes, educacion } = arr[ a ];
        y.ref = handleMore( ref, Ref );
        y.redes = handleMore( redes, Redes );
        y.educacion = handleMore( educacion, Edu );
      }
      if ( arr[ a ].idioma ) {
        const { idioma } = arr[ a ];
        y.idioma = handleMore( idioma, Idioma );
      }
    }
    r.push( y );
  }
  return r;
};

const handleMore = ( agg, g ) => {
  let z = []
  for ( let w = 0; w < agg.length; w++ ) {
    let r = new g( agg[ w ] );
    z.push( r );
  }
  return z;
}

module.exports = {
  handleNew
};
