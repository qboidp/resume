const Users = require("../models/Users");
const fs = require("fs");
const path = require("path");
const FormData = require("form-data");
const { API } = require("../helpers/axios");

const handleBB = async (files) => {
  if (files) {
    let file_path = files.foto.path;
    try {
      const file = fs.createReadStream(file_path);
      const form = new FormData();
      form.append("key", process.env.APP_KEY);
      form.append("image", file);
      const res = await API.post("/upload", form);
      if (res) {
        fs.unlink(file_path, (err) => {
          console.log("err", err);
        });
        return res.data.data.image.url;
      }
    } catch (error) {
      console.log(error);
    }
  } else {
    console.log(error);
  }
};

module.exports = handleBB;
