require( "dotenv" ).config( { path: "variable.env" } );
const FormData = require( "form-data" );
const fs = require( "fs" );

const { BB } = require( "../../config/bb" );


const handleBB = async ( files ) => {
  if ( files ) {
    let file_path = files.file.path;
    try {
      const file = fs.createReadStream( file_path );
      const form = new FormData();
      form.append( "key", process.env.APP_KEY );
      form.append( "image", file );
      const res = await BB.post( "/upload", form );
      if ( res ) {
        console.log( res.data.data.image.url );
        fs.unlink( file_path, ( err ) => {
          console.log( "err", err );
        } );
        return res.data.data.image.url;
      }
    } catch ( error ) {
      console.log( error );
    }
  } else {
    return '';
  }
};

module.exports = {
  handleBB
};
