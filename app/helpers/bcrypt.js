require( "dotenv" ).config( { path: "variable.env" } );
const bcryptjs = require( 'bcryptjs' );
const jwt = require( "jsonwebtoken" );

const comparePass = async ( pass, data ) => {
  return await bcryptjs.compare( pass, data );
};

const decodeSign = ( token ) => {
  return jwt.decode( token, null );
};

const hashPass = async ( pass ) => {
  const salt = await bcryptjs.genSalt( 10 );
  return await bcryptjs.hash( pass, salt );
}

const tokenSign = async ( user ) => {
  return jwt.sign( { id: user._id }, process.env.JWT_SECRETO, {
    expiresIn: "2h",
  } );
};

const verifyToken = async ( token ) => {
  try {
    return jwt.verify( token, process.env.JWT_SECRETO );
  } catch ( e ) {
    return null;
  }
};


module.exports = {
  comparePass,
  hashPass,
  tokenSign,
  decodeSign,
  verifyToken
};
