require( "dotenv" ).config( { path: "variable.env" } );
const mongoose = require( "mongoose" );

const conectarDB = () => {
  const DB_URL = process.env.DB_MONGO;
  console.log(DB_URL)
  mongoose.connect(
    DB_URL,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
    ( err, res ) => {
      if ( !err ) {
        console.log( "**** CONEXION CORRECTA ****" );
      } else {
        console.log( "***** ERROR DE CONEXION ****" );
      }
    }
  );
};

module.exports = { conectarDB };
