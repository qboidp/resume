require( "dotenv" ).config( { path: "variable.env" } );
const axios = require( "axios" );

const BB = axios.create( { baseURL: process.env.APP_BB } );

module.exports = { BB };
