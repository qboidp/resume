require( "dotenv" ).config( { path: "variables.env" } );
const express = require( "express" );
const cors = require( "cors" );
const { conectarDB } = require( "./config/db" );
const bodyParser = require( "body-parser" );
const port = process.env.PORT || 4000;
const path = require( "path" );
const app = express();
//
conectarDB();
app.use( cors() );
app.use( express.json() );
app.use( bodyParser.json() );
app.use( bodyParser.urlencoded( { extended: true } ) );
app.use( express.static( "public" ) );
app.use( "/api/1.1", require( "./app/routes" ) );
//
if ( process.env.NODE_ENV === "production" ) {
  app.use( express.static( "build" ) );
  app.get( "*", ( req, res ) =>
    res.sendFile( path.resolve( __dirname, "build", "index.html" ) )
  );
}
app.listen( port, "0.0.0.0", () => {
  console.log( `El servidor esta funcionando en el puerto ${ port }` );
  // main();
} );
